package model;

import java.util.Date;
import model.interfaces.IMatch;

/**
 * @author Claudiu Fecheta
 *
 */
public class Match implements IMatch {
	private final String matchID;
	private Team team1;
	private Team team2;
	private String role;
	private League league;
	private Date date;
	private String time;
	private String place;
	private int distance;
	private int matchFee;
	private Result result;
	
	public Match(String id) {
		this.matchID = id;
	}

	
	@Override
	public void setTeam1(Team team1) {
		this.team1 = team1;
	}

	@Override
	public Team getTeam1() {
		return this.team1;
	}

	@Override
	public void setTeam2(Team team2) {
		this.team2 = team2;
	}

	@Override
	public Team getTeam2() {
		return this.team2;
	}

	@Override
	public League getLeague() {
		return this.league;
	}

	@Override
	public Date getDate() {
		return this.date;
	}

	@Override
	public String getTime() {
		return this.time;
	}

	@Override
	public void setPlace(String place) {
		this.place = place;
	}

	@Override
	public String getPlace() {
		return this.place;
	}

	@Override
	public void setDistance(int distance) {
		this.distance = distance;
	}

	@Override
	public int getDistance() {
		return this.distance;
	}

	@Override
	public Result getResult() {
		return this.result;
	}

	@Override
	public String getMatchID() {
		return this.matchID;
	}


	@Override
	public String getRole() {
		return this.role;
	}


	@Override
	public void setRole(String role) {
		this.role = role;
	}


	@Override
	public int getMatchFee() {
		return this.matchFee;
	}


	@Override
	public void setMatchFee(int fee) {
		this.matchFee = fee;
	}


	@Override
	public void setLeague(League league) {
		this.league = league;
	}


	@Override
	public void setResult(Result result) {
		this.result = result;
	}


	@Override
	public void setDate(Date date) {
		this.date = date;
	}


	@Override
	public void setTime(String time) {
		this.time = time;
	}

}
