package model.interfaces;

import java.util.Date;

import model.League;
import model.Result;
import model.Team;

/**
 * @author Claudiu Fecheta
 *
 */
public interface IMatch {
	
	/**
	 * Returns the match ID
	 * @return match ID
	 */
	String getMatchID();
		
	/**
	 * Sets the first team of the match.
	 * @param team first team.
	 */
	void setTeam1(Team team);
	
	/**
	 * Returns the first team.
	 * @return the first team.
	 */
	Team getTeam1();
	
	/**
	 * Sets the second team of the match.
	 * @param team second team.
	 */
	void setTeam2(Team team);
	
	/**
	 * Returns the second team.
	 * @return the second team.
	 */
	Team getTeam2();
	
	/**
	 * Returns the name of the league of the match.
	 * @return the league name.
	 */
	League getLeague();
	
	/**
	 * Sets the league of the match.
	 * @param league 
	 */
	void setLeague(League league);
	
	/**
	 * Returns the date of the match
	 * @return the date of the match
	 */
	Date getDate();
	
	/**
	 * sets the date of the match
	 * @param date
	 */
	void setDate(Date date);
		
	/**
	 * Returns the time of the match
	 * @return the time of the match
	 */
	String getTime();
	
	/**
	 * sets the match time
	 * @param time
	 */
	void setTime(String time);
	
	/**
	 * Sets the place of the match.
	 * @param the name of the place.
	 */
	void setPlace(String place);
	
	/**
	 * Returns the name of the place.
	 * @return the name of the place.
	 */
	String getPlace();
	
	/**
	 * Sets the distance in km from the pitch.
	 * @param distance from the pitch.
	 */
	void setDistance(int distance);
	
	/**
	 * Returns the distance from the pitch.
	 * @return the distance from the pitch.
	 */
	int getDistance();
	
	/**
	 * Returns the stats of the match
	 * @return the stats of the match.
	 */
	Result getResult();
	
	/**
	 * Sets the result of the match
	 * @param result
	 */
	void setResult(Result result);
	
	/**
	 * Returns the ref role in the match
	 * @return ref role
	 */
	String getRole();
	
	/**
	 * Sets the ref role in the match
	 * @param role 
	 */
	void setRole(String role);
	
	/**
	 * Returns the fee for the match
	 * @return match fee
	 */
	int getMatchFee();
	
	/**
	 * Sets the match fee
	 * @param fee
	 */
	void setMatchFee(int fee);
	

}
