package model.interfaces;

/**
 * @author Claudiu Fecheta
 *
 */
public interface ITeam {
	
	/**
	 * Sets the name of the team
	 * @param teamName
	 */
	void setTeamName(String teamName);
	
	/**
	 * @return the name of the team
	 */
	String getTeamName();
	
	

}
