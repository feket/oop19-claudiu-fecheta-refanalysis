package model.interfaces;

/**
 * @author Claudiu Fecheta
 *
 */
public interface IResult {
	
	/**
	 * @return number of goals for the first team
	 */
	int getGoals1();
		
	/**
	 * 
	 * @return number of goals of second team
	 */
	int getGoals2();
	
	/**
	 * 
	 * @return number of yellow cards for the first team 
	 */
	int getYellowCards1();
	
	/**
	 * 
	 * @param yCards1 number of yellow cards for the first team
	 */
	void setYellowCards1(int yCards1);
	
	/**
	 * 
	 * @return number of yellow cards for the second team
	 */
	int getYellowCards2();
	
	/**
	 * 
	 * @param yCards2 number of yellow cards for the second team
	 */
	void setYellowCards2(int yCards2);
	
	/**
	 * 
	 * @return number of red cards for the first team
	 */
	int getRedCards1();
	
	/**
	 * 
	 * @param rCards1 number of red cards for the first team
	 */
	void setRedCards1(int rCards1);
	
	/**
	 * 
	 * @return number of red cards for the second team
	 */
	int getRedCards2();
	
	/**
	 * 
	 * @param rCards2 number of red cards for the second team
	 */
	void setRedCards2(int rCards2);
	
	/**
	 * 
	 * @return number of penalty kicks for the first team
	 */
	int getPenaltyKicks1();
	
	/**
	 * 
	 * @param penalty1 number of penalty kicks for the first team
	 */
	void setPenaltyKicks1(int penalty1);
	
	/**
	 * 
	 * @return number of penalty kicks for the second team
	 */
	int getPenaltyKicks2();
	
	/**
	 * 
	 * @param penalty2 number of penalty kicks for the second team
	 */
	void setPenaltyKicks2(int penalty2);
}
