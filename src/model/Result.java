package model;

import model.interfaces.IResult;

/**
 * @author Claudiu Fecheta
 *
 */
public class Result implements IResult {
	
	private final int goals1;
	private final int goals2;
	private int yCards1;
	private int yCards2;
	private int rCards1;
	private int rCards2;
	private int penaltyKicks1;
	private int penaltyKicks2; 
	
	
	public Result(int g1, int g2) {
		this.goals1 = g1;
		this.goals2 = g2;	
	}
	

	@Override
	public int getGoals1() {
		return this.goals1;
	}

	@Override
	public int getGoals2() {
		return this.goals2;
	}

	@Override
	public int getYellowCards1() {
		return this.yCards1;
	}

	@Override
	public void setYellowCards1(int yCards1) {
		this.yCards1 = yCards1;
	}

	@Override
	public int getYellowCards2() {
		return this.yCards2;
	}

	@Override
	public void setYellowCards2(int yCards2) {
		this.yCards2 = yCards2;
	}

	@Override
	public int getRedCards1() {
		return this.rCards1;
	}

	@Override
	public void setRedCards1(int rCards1) {
		this.rCards1 = rCards1;
	}

	@Override
	public int getRedCards2() {
		return this.rCards2;
	}

	@Override
	public void setRedCards2(int rCards2) {
		this.rCards2 = rCards2;
	}

	@Override
	public int getPenaltyKicks1() {
		return this.penaltyKicks1;
	}

	@Override
	public void setPenaltyKicks1(int penalty1) {
		this.penaltyKicks1 = penalty1;
	}

	@Override
	public int getPenaltyKicks2() {
		return this.penaltyKicks2;
	}

	@Override
	public void setPenaltyKicks2(int penalty2) {
		this.penaltyKicks2 = penalty2;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + goals1;
		result = prime * result + goals2;
		result = prime * result + penaltyKicks1;
		result = prime * result + penaltyKicks2;
		result = prime * result + rCards1;
		result = prime * result + rCards2;
		result = prime * result + yCards1;
		result = prime * result + yCards2;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Result other = (Result) obj;
		if (goals1 != other.goals1)
			return false;
		if (goals2 != other.goals2)
			return false;
		if (penaltyKicks1 != other.penaltyKicks1)
			return false;
		if (penaltyKicks2 != other.penaltyKicks2)
			return false;
		if (rCards1 != other.rCards1)
			return false;
		if (rCards2 != other.rCards2)
			return false;
		if (yCards1 != other.yCards1)
			return false;
		if (yCards2 != other.yCards2)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Result [goals1=" + goals1 + ", goals2=" + goals2 + ", yCards1=" + yCards1 + ", yCards2=" + yCards2
				+ ", rCards1=" + rCards1 + ", rCards2=" + rCards2 + ", penaltyKicks1=" + penaltyKicks1
				+ ", penaltyKicks2=" + penaltyKicks2 + "]";
	}
	
	

}
