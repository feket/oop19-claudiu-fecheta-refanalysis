package model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import model.interfaces.IMatch;

/**
 * @author Claudiu Fecheta
 *
 */

public class Model extends HashSet<Match> {
	
	private static final long serialVersionUID = 188513586678696016L;
	private static List<IMatch> matches;

	/**
	 * Inizialize the variable matches.
	 */
	public Model() {
		matches = new ArrayList<IMatch>();
	}	

	/**
	 * 
	 * @return the matches list
	 */
	public static List<IMatch> getLista() {
		return matches;
	}
	
	/**
	 * Transforms the date from string to date type
	 * @param stringDate
	 * @return date in Date type
	 */
	public static Date getDateFromString(String stringDate) {
		DateFormat format = new SimpleDateFormat("dd MMMM yyyy", Locale.ITALIAN);
		Date date;
		try {
			date = format.parse(stringDate);
			return date;
		} catch (ParseException e) {
			return null;
		}		
	}

}

