package model;

/**
 * The enumeration of the leagues in italian football
 * 
 * @author Claudiu Fecheta
 */

public enum League {
	
	SERIE_A("Serie A"),
	
	SERIE_B("Serie B"),
	
	LEGAPRO("LegaPro"),
	
	SERIE_D("Serie D"),
	
	ECCELLENZA("Eccellenza"),
	
	PROMOZIONE("Promozione"),
	
	PRIMA_CATEGORIA("Prima Categoria"),
	
	SECONDA_CATEGORIA("Seconda Categoria"),
	
	TERZA_CATEGORIA("Terza Categoria"),
	
	JUNIORES_REGIONALI("Juniores Regionali"),
	
	JUNIORES_NAZIONALI("Juniores Nazionali"),
	
	TORNEO_BERRETTI("Torneo Berretti"),
	
	CAMPIONATO_PRIMAVERA("Campionato Primavera"),
	
	ALLIEVI_NAZIONALI("Allievi Nazionali"),
	
	ALLIEVI_REGIONALI("Allievi Regionali"),
	
	ALLIEVI_PROVINCIALI("Allievi Provinciali"),
	
	GIOVANISSIMI_NAZIONALI("Giovanissimi Nazionali"),
	
	GIOVANISSIMI_REGIONALI("Giovanissimi Regionali"),
	
	GIOVANISSIMI_PROVINCIALI("Giovanissimi Provinciali"),
	
	SERIE_A_FEMMINILE("Serie A Femminile"),
	
	SERIE_B_FEMMINILE("Serie B Femminile"),
	
	SERIE_C_FEMMINILE("Serie C Femminile");
	
	private final String name;
	
	private League(final String name) {
		this.name = name;
	}
	
	/**
	 * @return the name of the league
	 */
	public String getName() {
		return this.name;
	}	

}
