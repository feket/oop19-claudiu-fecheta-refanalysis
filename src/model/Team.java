package model;

import model.interfaces.ITeam;

/**
 * @author Claudiu Fecheta
 *
 */
public class Team implements ITeam {
	
	private String teamName;

	/* sets the name of the team
	 * @param teamName 
	 */
	@Override
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	/* returns the name of the team
	 * @return teamName
	 */
	@Override
	public String getTeamName() {
		return this.teamName;
	}

}
