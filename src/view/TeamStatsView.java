package view;

import java.awt.BorderLayout;
import java.text.SimpleDateFormat;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import model.Model;
import model.interfaces.IMatch;

/**
 * @author Claudiu Fecheta
 *
 */
public class TeamStatsView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5835606858525802067L;
	private static final int MATCH_NUMBER_INDEX = 1;
	private static final int WON_MATCHES_INDEX = 2;
	private static final int LOST_MATCHES_INDEX = 3;
	private static final int TIE_MATCHES_INDEX = 4;
	private static final int YELLOW_CARDS_INDEX = 5;
	private static final int RED_CARDS_INDEX = 6;
	private static final int PENALTY_KICKS_INDEX = 7;
	private static final int COL_MATCH = 0;
	private static final int COL_DATE_TIME = 1;
	private static final int COL_LEAGUE = 2;
	private static final int COL_RESULT = 3;
	private static final int STATS_ROWS = 3;
	private static final int HOME_ROW = 0;
	private static final int AWAY_ROW = 1;
	private static final int TOTAL_ROW = 2;
	private final JTextField text = new JTextField(15);
	private final JPanel panelLabel = new JPanel();
	private final JButton okButton = new JButton("Ok");
	private final JButton exitButton = new JButton("Exit");
	private final JPanel panelButtons = new JPanel();	
	
	private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	private JTable tableStats;
	private JTable tableMatches;
	private final JPanel headerPanel = new JPanel();
	private JLabel lblHeader;
	private final JPanel tableStatsPanel = new JPanel(new BorderLayout());
	private final JPanel tableMatchesPanel = new JPanel(new BorderLayout());
	//field used to populate the tables rows and columns
	private Object[][] stats; 
	private String[] columnNamesStats = {"", "Matches", "Win", "Loss", "Tie", "Yellow Cards", "RedCards", "Penalty Kicks"};
	private String[] rowNamesStats = {"HOME", "AWAY", "TOTAL"};
	private Object[][] teamMatches; 
	private String[] columnNamesMatches = {"Match", "Date - Time", "League", "Result"};
	//arrays used to save the data of every single column
	private int[] nMatches = new int[STATS_ROWS];
	private int[] nWon = new int[STATS_ROWS];
	private int[] nTies = new int[STATS_ROWS];
	private int[] nLoss = new int[STATS_ROWS];
	private int[] nYCards = new int[STATS_ROWS];
	private int[] nRCards = new int[STATS_ROWS];
	private int[] nPenaltyKicks = new int[STATS_ROWS];	
	
	private JPanel panelSearchWindow;
	private JPanel panelData;
	
	private boolean isInputCorrect;
	
	public TeamStatsView() {
		this.setSize(300, 150);
		this.setTitle("Team's statistics");
		panelLabel.add(new JLabel("Insert team's name: "));		
		panelLabel.add(text);
		panelButtons.add(okButton);
		panelButtons.add(exitButton);
		panelSearchWindow = new JPanel(new BorderLayout());
		panelSearchWindow.add(panelLabel, BorderLayout.CENTER);
		panelSearchWindow.add(panelButtons, BorderLayout.SOUTH);
		panelData = new JPanel();
		panelData.setLayout(new BoxLayout(panelData, BoxLayout.Y_AXIS));
		
		okButton.addActionListener(l -> {
			 populateTables();
			 if(isInputCorrect) {
				 headerPanel.add(lblHeader);
				 tableStats = new JTable(stats, columnNamesStats);
				 tableStatsPanel.add(tableStats.getTableHeader(), BorderLayout.PAGE_START);
				 tableStatsPanel.add(tableStats, BorderLayout.CENTER);
				 tableMatches = new JTable(teamMatches, columnNamesMatches);
				 tableMatchesPanel.add(tableMatches.getTableHeader(), BorderLayout.PAGE_START);
				 tableMatchesPanel.add(tableMatches, BorderLayout.CENTER);
				 panelData.add(headerPanel);			 
				 panelData.add(tableStatsPanel);
				 panelData.add(tableMatchesPanel);
				 this.getContentPane().add(panelData);
				 this.setLocation(300, 200);
				 this.setSize(800, 600);
				 panelSearchWindow.setVisible(false);
				 this.setVisible(true);
			 }			 
		});
		
		exitButton.addActionListener(l -> {
			this.setVisible(false);
		});
		
		this.getContentPane().add(panelSearchWindow);
		this.setLocation(300, 200);
		this.setVisible(true);
	}
	
	private void populateTables() {
		int rows = 0;
		int numRow = 0;
		isInputCorrect = true;
		for(int i = 0; i < STATS_ROWS; i++) {
			nMatches[i] = 0;
			nWon[i] = 0;
			nTies [i] = 0;
			nLoss[i] = 0;
			nYCards[i] = 0;
			nRCards[i] = 0;
			nPenaltyKicks[i] = 0;
		}
		if (text.getText().length() == 0) {
			JOptionPane.showMessageDialog(TeamStatsView.this,
				"Empty text", "Incorrect data!",
				JOptionPane.ERROR_MESSAGE);
			isInputCorrect = false;
		} else {				
			for (IMatch m : Model.getLista()) {
				if ((m.getTeam1().getTeamName().equalsIgnoreCase(text.getText())) || 
						(m.getTeam2().getTeamName().equalsIgnoreCase(text.getText()))) {
					rows++;
				}
			}				
			if(rows == 0) {
				JOptionPane.showMessageDialog(TeamStatsView.this,
						"The team name doesn't exist", "Incorrect data!",
						JOptionPane.ERROR_MESSAGE);
				isInputCorrect = false;
			} else {
				lblHeader = new JLabel(text.getText().toUpperCase());
				teamMatches = new Object[rows][columnNamesMatches.length];
				for (IMatch m : Model.getLista()) {
					if ((m.getTeam1().getTeamName().equalsIgnoreCase(text.getText())) || 
							(m.getTeam2().getTeamName().equalsIgnoreCase(text.getText()))) {
						
						teamMatches[numRow][COL_MATCH] = m.getTeam1().getTeamName() + 
								" - " + m.getTeam2().getTeamName();
						teamMatches[numRow][COL_DATE_TIME] = formatter.format(m.getDate()) + 
								" at " + m.getTime();
						teamMatches[numRow][COL_LEAGUE] = m.getLeague();
						teamMatches[numRow][COL_RESULT] = m.getResult().getGoals1() + 
								" - " + m.getResult().getGoals2();
						numRow++;
						if (m.getTeam1().getTeamName().equalsIgnoreCase(text.getText())) {
							nMatches[HOME_ROW] ++;
							nYCards[HOME_ROW] += m.getResult().getYellowCards1();
							nRCards[HOME_ROW] += m.getResult().getRedCards1();
							nPenaltyKicks[HOME_ROW] += m.getResult().getPenaltyKicks1();
							if (m.getResult().getGoals1() > m.getResult().getGoals2()) {
								nWon[HOME_ROW] ++;
							} else {
								if (m.getResult().getGoals1() == m.getResult().getGoals2()) {
									nTies[HOME_ROW] ++;
								} else {
									nLoss[HOME_ROW] ++;
								}
							}
						} else {
							if (m.getTeam2().getTeamName().equalsIgnoreCase(text.getText())) {
								nMatches[AWAY_ROW] ++;
								nYCards[AWAY_ROW] += m.getResult().getYellowCards2();
								nRCards[AWAY_ROW] += m.getResult().getRedCards2();
								nPenaltyKicks[AWAY_ROW] += m.getResult().getPenaltyKicks2();
								if (m.getResult().getGoals2() > m.getResult().getGoals1()) {
									nWon[AWAY_ROW] ++;
								} else {
									if (m.getResult().getGoals2() == m.getResult().getGoals1()) {
										nTies[AWAY_ROW] ++;
									} else {
										nLoss[AWAY_ROW] ++;
									}
								}
							}
						}
						nMatches[TOTAL_ROW] = nMatches[HOME_ROW] + nMatches[AWAY_ROW];
						nYCards[TOTAL_ROW] = nYCards[HOME_ROW] + nYCards[AWAY_ROW];
						nRCards[TOTAL_ROW] = nRCards[HOME_ROW] + nRCards[AWAY_ROW];
						nPenaltyKicks[TOTAL_ROW] = nPenaltyKicks[HOME_ROW] + nPenaltyKicks[AWAY_ROW];
						nWon[TOTAL_ROW] = nWon[HOME_ROW] + nWon[AWAY_ROW];
						nTies[TOTAL_ROW] = nTies[HOME_ROW] + nTies[AWAY_ROW];
						nLoss[TOTAL_ROW] = nLoss[HOME_ROW] + nLoss[AWAY_ROW];
						stats = new Object[rowNamesStats.length][columnNamesStats.length];
						for(int i = 0; i < rowNamesStats.length; i++) {
							stats[i][0] = rowNamesStats[i];
							stats[i][MATCH_NUMBER_INDEX] = nMatches[i];
							stats[i][WON_MATCHES_INDEX] = nWon[i];
							stats[i][LOST_MATCHES_INDEX] = nLoss[i];
							stats[i][TIE_MATCHES_INDEX] = nTies[i];
							stats[i][YELLOW_CARDS_INDEX] = nYCards[i];
							stats[i][RED_CARDS_INDEX] = nRCards[i];
							stats[i][PENALTY_KICKS_INDEX] = nPenaltyKicks[i];
						}
						
					}
				}
			}
		}
		
	}

}
