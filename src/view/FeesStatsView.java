package view;

import java.awt.BorderLayout;
import java.text.SimpleDateFormat;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;

import model.Model;
import model.interfaces.IMatch;

/**
 * @author Claudiu Fecheta
 *
 */
public class FeesStatsView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8716628305185076341L;
	private static final int MATCH_INDEX = 0;
	private static final int DATE_TIME_INDEX = 1;
	private static final int LEAGUE_INDEX = 2;
	private static final int FEE_INDEX = 3;
	private static final int HEIGHT = 300;
	private static final int WIDTH = 900;
	private static final int POS_X = 150;
	private static final int POS_Y = 150;
	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	private final JTable table;
	private final JPanel panelTable;
	private final JPanel panelTot;
	private final JLabel lblTotAmountFees; 
	private final Object[][] data; 
	String[] columnNames = {"Match", "Date and time", "League", "Fee"};
	int totAmountFees;
           
	public FeesStatsView() {
		super("Matches Fees");
		this.setSize(WIDTH, HEIGHT);
		this.setLocation(POS_X, POS_Y);		
		this.getContentPane().setLayout(new BorderLayout());
		panelTable = new JPanel(new BorderLayout());
		panelTot = new JPanel();
		data = new Object[Model.getLista().size()][columnNames.length];
		addDataToTable();
		table = new JTable(data, columnNames);
		lblTotAmountFees = new JLabel("Total amount Fees: " + totAmountFees + "�");
		panelTot.add(lblTotAmountFees);
		panelTable.add(table.getTableHeader(), BorderLayout.PAGE_START);
		panelTable.add(table, BorderLayout.CENTER);
		this.getContentPane().add(panelTable, BorderLayout.CENTER);
		this.getContentPane().add(panelTot, BorderLayout.SOUTH);
		this.setVisible(true);
	}
	
	private void addDataToTable() {
		totAmountFees = 0;
		for (IMatch m : Model.getLista()) {
			data[Model.getLista().indexOf(m)][MATCH_INDEX] = m.getTeam1().getTeamName() + " - " + m.getTeam2().getTeamName();
			data[Model.getLista().indexOf(m)][DATE_TIME_INDEX] = formatter.format(m.getDate()) + " at " + m.getTime();
			data[Model.getLista().indexOf(m)][LEAGUE_INDEX] = m.getLeague();
			data[Model.getLista().indexOf(m)][FEE_INDEX] = m.getMatchFee() + "�";
			totAmountFees += m.getMatchFee();
		}
	}

}
