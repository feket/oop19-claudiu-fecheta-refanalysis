package view;

import controller.Controller;
import view.AddMatch;
import view.interfaces.IView;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.*;

/**
 * @author Claudiu Fecheta
 *
 */

public class MainView extends JFrame implements IView{
	
	private static final long serialVersionUID = 1L;
	private final static String TITLE = "RefAnalysis";
	private final static int HEIGHT = 330;
	private final static int WIDTH = 590;
	private final static int BUTTONS_PANEL_HEIGHT = 600;
	private final static int BUTTONS_PANEL_WIDTH = 200;
	private Controller controller;
	private final JButton matchesListButton = new JButton("Show all Matches");
	private final JButton teamStatsButton = new JButton("Teams statistics");
	private final JButton addButton = new JButton("Add new Match");
	private final JButton matchFeesButton = new JButton("Fees statistics");
	private final JButton refStatisticsButton = new JButton("Referee statistics");
	private final JSplitPane splitPane = new JSplitPane(
			JSplitPane.HORIZONTAL_SPLIT);
	final JPanel buttonsPanel = new JPanel();
	private Font font;
	private final JDesktop desktop;
	
	private static ImageIcon image = createImageIcon("/arbitro.jpg");
	
	public MainView() throws ClassNotFoundException, IOException {
		super(TITLE);
		controller = new Controller();
		this.setSize(WIDTH, HEIGHT);
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this.setLocation(50, 50);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(final WindowEvent e) {
				quitHandler();
			}
		});
		this.setLayout(new BorderLayout());
		
		buttonsPanel.setSize(BUTTONS_PANEL_WIDTH, BUTTONS_PANEL_HEIGHT);
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.Y_AXIS));
		buildLayout();
		buttonHandlers();
		
		Image scaledImage = image.getImage().getScaledInstance(400, 300, Image.SCALE_DEFAULT);
		image.setImage(scaledImage);
		
		desktop = new JDesktop(image.getImage());
		
		splitPane.setContinuousLayout(true);
		splitPane.setLeftComponent(buttonsPanel);
		splitPane.setRightComponent(desktop);
		this.getContentPane().add(splitPane);
		this.setResizable(false);
		this.setVisible(true);
	}
	
	private void buttonHandlers() {
		this.addButton.addActionListener(l -> new AddMatch());
		this.matchesListButton.addActionListener(l -> new MatchesListView());
		this.teamStatsButton.addActionListener(l -> new TeamStatsView());
		this.matchFeesButton.addActionListener(l -> new FeesStatsView());
		this.refStatisticsButton.addActionListener(l -> new RefereeStatsView());
	}
	
	private void quitHandler() {
		final int n = JOptionPane.showConfirmDialog(this,
				"Do you really want to quit?", "Quitting..",
				JOptionPane.YES_NO_OPTION);
		if (n == JOptionPane.YES_OPTION) {
			controller.commandQuit();
		}
	}
	
	private void buildLayout() {
		
		font = new Font("Verdana", Font.BOLD, 14);
		
		final JPanel panelButAdd = new JPanel();		
		addButton.setFont(font);
		panelButAdd.add(addButton, BorderLayout.CENTER);
		
		final JPanel panelButEvents = new JPanel();	
		matchesListButton.setFont(font);
		panelButEvents.add(matchesListButton, BorderLayout.CENTER);
		
		final JPanel panelTeamSearch = new JPanel();
		teamStatsButton.setFont(font);
		panelTeamSearch.add(teamStatsButton, BorderLayout.CENTER);
		
		final JPanel panelMatchFees = new JPanel();
		matchFeesButton.setFont(font);
		panelMatchFees.add(matchFeesButton, BorderLayout.CENTER);
		
		final JPanel panelRefStatistics = new JPanel();
		refStatisticsButton.setFont(font);
		panelRefStatistics.add(refStatisticsButton, BorderLayout.CENTER);
		
		buttonsPanel.add(panelButAdd);
		buttonsPanel.add(panelButEvents);
		buttonsPanel.add(panelTeamSearch);
		buttonsPanel.add(panelMatchFees);
		buttonsPanel.add(panelRefStatistics);
		
	}
	
	private static ImageIcon createImageIcon(final String path) {
		final java.net.URL imgURL = MainView.class.getResource(path);

		if (imgURL == null) {
			System.err.println("Couldn't find image file: " + path);
			return null;

		} else {
			return new ImageIcon(imgURL);
		}
	}
	
	@Override
	public void attachViewObserver(Controller controller) {
		this.controller = controller;
	}
} 
