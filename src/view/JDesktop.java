package view;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JDesktopPane;

/**
 * @author Claudiu Fecheta
 *
 */
public class JDesktop extends JDesktopPane {

	/**
	 * 
	 */
	private static final long serialVersionUID = 413197045556543631L;
	private final Image img;
	
	public JDesktop(final Image img) {
		this.img = img;
	}
	
	public void paintComponent(final Graphics g) {
		
		final Dimension panelSize = getSize();
		final int width = img.getWidth(this);
		final int height = img.getHeight(this);

		for (int y = 0; y < panelSize.height; y += height) {
			for (int x = 0; x < panelSize.width; x += width) {
				g.drawImage(img, x, y, this);
			}
		}
	}
	
	

}
