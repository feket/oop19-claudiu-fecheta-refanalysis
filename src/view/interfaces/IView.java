package view.interfaces;

import controller.Controller;

/**
 * @author Claudiu Fecheta
 *
 */
public interface IView {
	
	/**
	 * 
	 * @param controller
	 * 				the {@link IController} that will manage the view.
	 */
	void attachViewObserver(final Controller controller);

}
