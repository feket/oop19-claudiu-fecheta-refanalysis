package view.interfaces;

/**
 * @author Claudiu Fecheta
 *
 */
public interface IAddMatch {
	
	/**
	 * Removes all the entries from the view
	 */
	void clearFieldsValues();
	
	/**
	 * Removes all the error messages from the view
	 */
	void clearErrorValues();
	
	/**
	 * Checks all the input params
	 * @return true if all the params are correct otherwise false
	 */ 
	boolean checkInputParams();
	
	/**
	 * Saves the new match on the file of the matches
	 */
	void salvaSuFile();
	
	/**
	 * Builds the layout of the pre match tab
	 */
	void buildLayoutPre();
	
	/**
	 * Builds the layout of the post match tab
	 */
	void buildLayoutPost();
	
	/**
	 * Checks the post match params
	 * @return true if the post match params aren't all 0
	 */
	boolean checkPostMatchParams();

}
