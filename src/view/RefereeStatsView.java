package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DecimalFormat;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import model.Model;
import model.interfaces.IMatch;

/**
 * @author Claudiu Fecheta
 *
 */
public class RefereeStatsView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6040946807865513771L;
	private final JPanel statsPanel = new JPanel();
	private int numMatches = 0;
	private int totGoals1 = 0;
	private int totGoals2 = 0;
	private int numYCards1 = 0;
	private int numYCards2 = 0;
	private int numRCards1 = 0;
	private int numRCards2 = 0;
	private int numPenaltyKicks1 = 0;
	private int numPenaltyKicks2 = 0;
	private int totDistance = 0;
	
	private	final JLabel lblNumMatches;
	private	final JLabel lblTotGoals1;
	private	final JLabel lblTotGoals2;
	private	final JLabel lblNumYCards1;
	private	final JLabel lblNumYCards2;
	private	final JLabel lblNumRCards1;
	private	final JLabel lblNumRCards2;
	private	final JLabel lblNumPenaltyKicks1;
	private	final JLabel lblNumPenaltyKicks2;
	private	final JLabel lblTotDistance;
	
	public RefereeStatsView() {
		super("Referee stats");
		this.setSize(550, 400);
		this.setLocation(100, 100);		
		getContentPane().setLayout(new BorderLayout());
		this.statsPanel.setLayout(new GridBagLayout());
		TitledBorder borderName = BorderFactory
				.createTitledBorder("Referee Stats");
		borderName.setTitleColor(Color.BLUE);
		borderName.setTitleFont(new Font("Serif", Font.BOLD, 16));
		borderName.setTitleJustification(TitledBorder.LEFT);
		this.statsPanel.setBorder(new CompoundBorder(borderName,
				new EmptyBorder(0, 30, 30, 30)));
		calculateStats();
		this.lblNumMatches = new JLabel("Total matches refereed: " + numMatches);
		this.lblTotGoals1 = new JLabel("Total HOME goals: " + totGoals1 + 
				" (Average per match : " + new DecimalFormat("#.##").format((double)totGoals1/numMatches) + ")");
		this.lblTotGoals2 = new JLabel("Total AWAY goals: " + totGoals2 + 
				" (Average per match : " + new DecimalFormat("#.##").format((double)totGoals2/numMatches) + ")");
		this.lblNumYCards1 = new JLabel("Total HOME yellow cards: " + numYCards1 + 
				" (Average per match : " + new DecimalFormat("#.##").format((double)numYCards1/numMatches) + ")");
		this.lblNumYCards2 = new JLabel("Total AWAY yellow cards: " + numYCards2 + 
				" (Average per match : " + new DecimalFormat("#.##").format((double)numYCards2/numMatches) + ")");
		this.lblNumRCards1 = new JLabel("Total HOME red cards: " + numRCards1 + 
				" (Average per match : " + new DecimalFormat("#.##").format((double)numRCards1/numMatches) + ")");
		this.lblNumRCards2 = new JLabel("Total AWAY red cards: " + numRCards2 + 
				" (Average per match : " + new DecimalFormat("#.##").format((double)numRCards2/numMatches) + ")");
		this.lblNumPenaltyKicks1 = new JLabel("Total HOME penalty kicks: " + numPenaltyKicks1 + 
				" (Average per match : " + new DecimalFormat("#.##").format((double)numPenaltyKicks1/numMatches) + ")");
		this.lblNumPenaltyKicks2 = new JLabel("Total AWAY penalty kicks: " + numPenaltyKicks2 + 
				" (Average per match : " + new DecimalFormat("#.##").format((double)numPenaltyKicks2/numMatches) + ")");
		this.lblTotDistance = new JLabel("Total distance traveled: " + totDistance + " km" +
				" (Average per match : " + new DecimalFormat("#.##").format((double)totDistance/numMatches) + ")");
		buildLayout();
		this.getContentPane().add(statsPanel, BorderLayout.CENTER);
		this.setVisible(true);
		
	}
	
	private void calculateStats() {
		for(IMatch m : Model.getLista()) {
			numMatches += 1;
			totGoals1 += m.getResult().getGoals1();
			totGoals2 += m.getResult().getGoals2();
			numYCards1 += m.getResult().getYellowCards1();
			numYCards2 += m.getResult().getYellowCards2();
			numRCards1 += m.getResult().getRedCards1();
			numRCards2 += m.getResult().getRedCards2();
			numPenaltyKicks1 += m.getResult().getPenaltyKicks1();
			numPenaltyKicks2 += m.getResult().getPenaltyKicks2();
			totDistance += m.getDistance();			
		}		
	}
	
	private void buildLayout() {
		GridBagConstraints gbc1 = new GridBagConstraints();
		
		//tot num matches
		gbc1.gridx = 0;
		gbc1.gridy = 0;
		gbc1.insets = new Insets(5, 0, 0, 10);
		gbc1.anchor = GridBagConstraints.LINE_END;
		statsPanel.add(lblNumMatches, gbc1);
		
		//tot num goals home
		gbc1.gridx = 0;
		gbc1.gridy = 1;
		gbc1.insets = new Insets(5, 0, 0, 10);
		gbc1.anchor = GridBagConstraints.LINE_END;
		statsPanel.add(lblTotGoals1, gbc1);
		
		//tot num goals away
		gbc1.gridx = 0;
		gbc1.gridy = 2;
		gbc1.insets = new Insets(5, 0, 0, 10);
		gbc1.anchor = GridBagConstraints.LINE_END;
		statsPanel.add(lblTotGoals2, gbc1);
		
		//tot num yellow cards home
		gbc1.gridx = 0;
		gbc1.gridy = 3;
		gbc1.insets = new Insets(5, 0, 0, 10);
		gbc1.anchor = GridBagConstraints.LINE_END;
		statsPanel.add(lblNumYCards1, gbc1);
		
		//tot num yellow cards away
		gbc1.gridx = 0;
		gbc1.gridy = 4;
		gbc1.insets = new Insets(5, 0, 0, 10);
		gbc1.anchor = GridBagConstraints.LINE_END;
		statsPanel.add(lblNumYCards2, gbc1);
		
		//tot num red cards home
		gbc1.gridx = 0;
		gbc1.gridy = 5;
		gbc1.insets = new Insets(5, 0, 0, 10);
		gbc1.anchor = GridBagConstraints.LINE_END;
		statsPanel.add(lblNumRCards1, gbc1);
			
		//tot num red cards away
		gbc1.gridx = 0;
		gbc1.gridy = 6;
		gbc1.insets = new Insets(5, 0, 0, 10);
		gbc1.anchor = GridBagConstraints.LINE_END;
		statsPanel.add(lblNumRCards2, gbc1);
		
		//tot num penalty kicks home
		gbc1.gridx = 0;
		gbc1.gridy = 7;
		gbc1.insets = new Insets(5, 0, 0, 10);
		gbc1.anchor = GridBagConstraints.LINE_END;
		statsPanel.add(lblNumPenaltyKicks1, gbc1);
				
		//tot num penalty kicks away
		gbc1.gridx = 0;
		gbc1.gridy = 8;
		gbc1.insets = new Insets(5, 0, 0, 10);
		gbc1.anchor = GridBagConstraints.LINE_END;
		statsPanel.add(lblNumPenaltyKicks2, gbc1);
		
		//tot distance 
		gbc1.gridx = 0;
		gbc1.gridy = 9;
		gbc1.insets = new Insets(5, 0, 0, 10);
		gbc1.anchor = GridBagConstraints.LINE_END;
		statsPanel.add(lblTotDistance, gbc1);
	}

}
