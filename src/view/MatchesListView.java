package view;

import java.awt.BorderLayout;
import java.text.SimpleDateFormat;
import javax.swing.JFrame;
import javax.swing.JTable;
import model.Model;
import model.interfaces.IMatch;

/**
 * @author Claudiu Fecheta
 *
 */
public class MatchesListView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -772295683576588649L;
	private static final int ID_INDEX = 0;
	private static final int MATCH_INDEX = 1;
	private static final int DATE_TIME_INDEX = 2;
	private static final int LEAGUE_INDEX = 3;
	private static final int RESULT_INDEX = 4;
	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	private final JTable table;
	private final Object[][] data; 
	String[] columnNames = {"ID", "Match", "Date and time", "League", "Result"};
           
	public MatchesListView() {
		super("Matches List");
		this.setSize(900, 300);
		this.setLocation(150, 150);		
		this.getContentPane().setLayout(new BorderLayout());
		data = new Object[Model.getLista().size()][columnNames.length];
		addDataToTable();
		table = new JTable(data, columnNames);
		this.add(table.getTableHeader(), BorderLayout.PAGE_START);
		this.getContentPane().add(table, BorderLayout.CENTER);
		this.setVisible(true);
	}
	
	private void addDataToTable() {
		for (IMatch m : Model.getLista()) {
			data[Model.getLista().indexOf(m)][ID_INDEX] = m.getMatchID();
			data[Model.getLista().indexOf(m)][MATCH_INDEX] = m.getTeam1().getTeamName() + " - " + m.getTeam2().getTeamName();
			data[Model.getLista().indexOf(m)][DATE_TIME_INDEX] = formatter.format(m.getDate()) + " at " + m.getTime();
			data[Model.getLista().indexOf(m)][LEAGUE_INDEX] = m.getLeague();
			data[Model.getLista().indexOf(m)][RESULT_INDEX] = m.getResult().getGoals1() + " - " + m.getResult().getGoals2();
		}
	}

}
