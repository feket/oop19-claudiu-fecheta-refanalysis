package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.TimePicker;

import main.Start;
import model.League;
import model.Match;
import model.Model;
import model.Result;
import model.Team;
import model.interfaces.IMatch;
import view.interfaces.IAddMatch;

/**
 * @author Claudiu Fecheta
 *
 */
public class AddMatch extends JFrame implements IAddMatch {

	private static final long serialVersionUID = 9008178921418817543L;
	private static final int BORDER = 15;
	private final JPanel preMatchDataPanel = new JPanel();
	private final JPanel postMatchPanel1 = new JPanel();
	private final JPanel postMatchPanel2 = new JPanel();
	private final String[] refRoles = {" ", "Arbitro Effettivo", "Assistente n.1", "Assistente n.2"};
	private final JTabbedPane addMatchTabs;
	private final JButton insert = new JButton("Insert");
	private final JButton cancel = new JButton("Clear");
	private final JPanel confirmButtonsPanel;
	private final JPanel preMatchPanel = new JPanel();
	
	//pre/post match fields
	private final JLabel lblMatchID = new JLabel("Match ID:");
	private final JTextField matchID = new JTextField(20);
	private final JLabel matchIDError = new JLabel("Please insert a match ID.");
	private final JLabel lblRole = new JLabel("Role:");
	private final JComboBox<String> refRole = new JComboBox<>(refRoles);
	private final JLabel roleError = new JLabel("Please select a role.");
	private final JLabel lblTeam1 = new JLabel("First Team:");
	private final JTextField team1Name = new JTextField(20);
	private final JLabel team1Error = new JLabel("Please insert a team name.");
	private final JLabel lblTeam2 = new JLabel("Second Team:");
	private final JTextField team2Name = new JTextField(20);
	private final JLabel team2Error = new JLabel("Please insert a team name.");
	private final JLabel lblLeague = new JLabel("League:");
	private final JComboBox<League> matchLeague = new JComboBox<>();
	private final JLabel lblMatchDate = new JLabel("Date:");
	private final DatePicker matchDate = new DatePicker();
	private final JLabel dateError = new JLabel("Please insert valid date.");
	private final JLabel lblMatchTime = new JLabel("Time:");
	private final TimePicker matchTime = new TimePicker();
	private final JLabel timeError = new JLabel("Please insert a value.");
	private final JLabel lblMatchLocation = new JLabel("Place:");
	private final JTextField matchLocation = new JTextField(20);
	private final JLabel placeError = new JLabel("Please insert a place.");
	private final JLabel lblMatchDistance = new JLabel("Distance:");
	private final JTextField matchDistance = new JTextField(20);
	private final JLabel distanceError = new JLabel("Please insert a distance.");
	private final JLabel lblMatchFee = new JLabel("Rimborso:");
	private final JTextField matchFee = new JTextField(20);
	private final JLabel feeError = new JLabel("Please insert a fee.");
	private final JLabel team1Goals = new JLabel("Goals:");
	private final SpinnerModel valueGoals1 = new SpinnerNumberModel(0, 0, 15, 1);
	private final JSpinner goals1Count = new JSpinner(valueGoals1);
	private final JLabel team2Goals = new JLabel("Goals:");
	private final SpinnerModel valueGoals2 = new SpinnerNumberModel(0, 0, 15, 1);
	private final JSpinner goals2Count = new JSpinner(valueGoals2);
	private final JLabel team1YCards = new JLabel("Yellow cards:");
	private final SpinnerModel valueYCards1 = new SpinnerNumberModel(0, 0, 15, 1);
	private final JSpinner yCards1Count = new JSpinner(valueYCards1);
	private final JLabel team2YCards = new JLabel("Yellow cards:");
	private final SpinnerModel valueYCards2 = new SpinnerNumberModel(0, 0, 15, 1);
	private final JSpinner yCards2Count = new JSpinner(valueYCards2);
	private final JLabel team1RCards = new JLabel("Red cards:");
	private final SpinnerModel valueRCards1 = new SpinnerNumberModel(0, 0, 15, 1);
	private	final JSpinner rCards1Count = new JSpinner(valueRCards1);
	private final JLabel team2RCards = new JLabel("Red cards:");
	private final SpinnerModel valueRCards2 = new SpinnerNumberModel(0, 0, 15, 1);
	private	final JSpinner rCards2Count = new JSpinner(valueRCards2);
	private final JLabel team1Penalty = new JLabel("Penalty shoots:");
	private final SpinnerModel valuePenalty1 = new SpinnerNumberModel(0, 0, 15, 1);
	private	final JSpinner penalty1Count = new JSpinner(valuePenalty1);
	private	final JLabel team2Penalty = new JLabel("Penalty shoots:");
	private	final SpinnerModel valuePenalty2 = new SpinnerNumberModel(0, 0, 15, 1);
	private	final JSpinner penalty2Count = new JSpinner(valuePenalty2);	
	
	public AddMatch() {
		super("Add new Match");
		this.setSize(550, 400);
		this.setLocation(100, 100);		
		getContentPane().setLayout(new BorderLayout());
		this.preMatchDataPanel.setBorder(new EmptyBorder(BORDER, BORDER, BORDER,
				BORDER));		
		this.preMatchDataPanel.setLayout(new GridBagLayout());
		this.postMatchPanel1.setLayout(new GridBagLayout());
		postMatchPanel1.setPreferredSize(new Dimension(250, 600) );
		this.postMatchPanel2.setLayout(new GridBagLayout());
		TitledBorder titleBorderPanel1 = new TitledBorder("Team 1");
		TitledBorder titleBorderPanel2 = new TitledBorder("Team 2");
		postMatchPanel1.setBorder(titleBorderPanel1);
		postMatchPanel2.setBorder(titleBorderPanel2);
		final JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, postMatchPanel1, postMatchPanel2);	
		buildLayoutPre();
		buildLayoutPost();
		confirmButtonsPanel = new JPanel(new FlowLayout());
		confirmButtonsPanel.add(insert);
		confirmButtonsPanel.add(cancel);
		preMatchPanel.setLayout(new BorderLayout());
		preMatchPanel.add(preMatchDataPanel, BorderLayout.NORTH);
		preMatchPanel.add(confirmButtonsPanel, BorderLayout.SOUTH);
		
		insert.addActionListener(e -> {
				boolean controllo = checkInputParams();
				if (controllo) {
					if(!checkPostMatchParams()) {
						JOptionPane.showMessageDialog(AddMatch.this,
								"All the post match fields are empty!", "Empty fields",
								JOptionPane.WARNING_MESSAGE);
					}
					int n = JOptionPane.showConfirmDialog(null,
							"Do you want to insert the new match?", "Match insert",
							JOptionPane.YES_NO_OPTION);
					if (n == JOptionPane.YES_OPTION) {
						salvaSuFile();
						setVisible(false);
					}
				}
		});		
		
		cancel.addActionListener(e -> {
				clearFieldsValues();
				clearErrorValues();
		});
		
		addMatchTabs = new JTabbedPane();
		addMatchTabs.add("Pre Partita", preMatchPanel);
		addMatchTabs.add("Post Partita", splitPane);
		this.getContentPane().add(addMatchTabs, BorderLayout.CENTER);
		this.setResizable(true);
		this.setVisible(true);
	}
	
	@Override
	public void clearFieldsValues() {		
		matchID.setText("");
		refRole.setSelectedIndex(0);
		team1Name.setText("");
		team2Name.setText("");
		matchLeague.setSelectedIndex(0);
		matchDate.setDateToToday();;
		matchTime.setText("");
		matchLocation.setText("");
		matchDistance.setText("");
		matchFee.setText("");
		
		goals1Count.setValue(0);
		goals2Count.setValue(0);
		yCards1Count.setValue(0);
		yCards2Count.setValue(0);
		rCards1Count.setValue(0);
		rCards2Count.setValue(0);
		penalty1Count.setValue(0);
		penalty2Count.setValue(0);		
	}
	
	@Override
	public void clearErrorValues() {
		matchIDError.setVisible(false);
		roleError.setVisible(false);
		team1Error.setVisible(false);
		team2Error.setVisible(false);
		dateError.setVisible(false);
		timeError.setVisible(false);
		placeError.setVisible(false);
		distanceError.setVisible(false);
		feeError.setVisible(false);
	}
	
	/**Check that the insert fields aren't empty
	 * 
	 * @return true if the insert fields aren't empty
	 */
	private boolean isNotNull() {
		
		boolean isNotNull = true;
		
		if(matchID.getText().length() == 0) {
			matchIDError.setVisible(true);
			isNotNull = false;
		}		
		if(refRole.getSelectedIndex() == 0) {
			roleError.setVisible(true);
			isNotNull = false;
		}		
		if(team1Name.getText().length() == 0) {
			team1Error.setVisible(true);
			isNotNull = false;
		}
		if(team2Name.getText().length() == 0) {
			team2Error.setVisible(true);
			isNotNull = false;
		}
		if(matchDate.getText().length() == 0) {
			dateError.setVisible(true);
			isNotNull = false;
		}
		if(matchTime.getText().length() == 0) {
			timeError.setVisible(true);
			isNotNull = false;
		}
		if(matchLocation.getText().length() == 0) {
			placeError.setVisible(true);
			isNotNull = false;
		}
		if(matchDistance.getText().length() == 0) {
			distanceError.setVisible(true);
			isNotNull = false;
		}
		if(matchFee.getText().length() == 0) {
			feeError.setVisible(true);
			isNotNull = false;
		}
		
		return isNotNull;
	}

	/**
	 * check if the match id already exists
	 * @return true if it doesn't exist
	 */
	private boolean checkMatchID() {
		for (IMatch match : Model.getLista()) {
			if (matchID.getText().equals(match.getMatchID())) {
				matchIDError.setText("Err. The ID already exists!");
				matchIDError.setVisible(true);
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Check if the two teams have the same name
	 * @return true if they don't
	 */
	private boolean checkTeamName() {		
		if (team1Name.getText().equals(team2Name.getText())) {
				team1Error.setText("Write two different team names!");
				team1Error.setVisible(true);
				return false;
			}		
		return true;
	}
	
	/**
	 * Check if the distance value is a number
	 * @return true if it's a number
	 */
	private boolean checkDistance() {			
		for (int i = 0; i < matchDistance.getText().length(); i++) {
		   if(!Character.isDigit(matchDistance.getText().charAt(i))) {
			   	distanceError.setText("Insert numbers 0-9");
			   	distanceError.setVisible(true);
			   	return false;
		   }
		}
		return true;
	}
	
	/**
	 * Check if the fee value is a number
	 * @return true if it's a number
	 */
	private boolean checkFee() {
		for (int i = 0; i < matchFee.getText().length(); i++) {
			if(!Character.isDigit(matchFee.getText().charAt(i))) {
				feeError.setText("Insert numbers 0-9");
				feeError.setVisible(true);
				return false;
			}
		}
		return true;
	}
	
	@Override
	public boolean checkInputParams() {
		clearErrorValues();
		return isNotNull() && checkMatchID() && checkTeamName() && checkDistance() && checkFee();
	}

	@Override
	public void salvaSuFile() {
		try {
			BufferedWriter write = new BufferedWriter(new FileWriter(Start.getFileName(), true));

			write.write(matchID.getText() + ";" + refRole.getSelectedItem() + ";"
					+ team1Name.getText() + ";" + team2Name.getText() + ";"
					+ matchLeague.getSelectedItem() + ";" + matchDate.getText() + ";"
					+ matchTime.getText() + ";" + matchLocation.getText() + ";" 
					+ matchDistance.getText() + ";" + matchFee.getText() + ";" 
					+ goals1Count.getValue() + ";" + goals2Count.getValue() + ";"
					+ yCards1Count.getValue() + ";" + yCards2Count.getValue() + ";"
					+ rCards1Count.getValue() + ";" + rCards2Count.getValue() + ";" 
					+ penalty1Count.getValue() + ";" + penalty2Count.getValue() + "\n");
			
			Team team1 = new Team();
			team1.setTeamName(team1Name.getText());
			Team team2 = new Team();
			team2.setTeamName(team2Name.getText());
			
			Result result = new Result((Integer) goals1Count.getValue(), (Integer) goals2Count.getValue());
			result.setYellowCards1((Integer) yCards1Count.getValue());
			result.setYellowCards2((Integer) yCards2Count.getValue());
			result.setRedCards1((Integer) rCards1Count.getValue());
			result.setRedCards2((Integer) rCards2Count.getValue());
			result.setPenaltyKicks1((Integer) penalty1Count.getValue());
			result.setPenaltyKicks1((Integer) penalty1Count.getValue());
			
			Match match = new Match(matchID.getText());
			match.setRole(refRole.getSelectedItem().toString());
			match.setTeam1(team1);
			match.setTeam2(team2);
			match.setLeague((League) matchLeague.getSelectedItem());
			Date date = Model.getDateFromString(matchDate.getText());
			match.setDate(date);
			match.setTime(matchTime.getText());
			match.setPlace(matchLocation.getText());
			match.setDistance((Integer.parseInt(matchDistance.getText())));
			match.setMatchFee(Integer.parseInt(matchFee.getText()));
			match.setResult(result);
			
			Model.getLista().add(match);
			write.close();
		} catch (IOException ex) {
			System.out.println("problem while saving the match in file");
		}
	}

	@Override
	public void buildLayoutPre() {
		GridBagConstraints gbc1 = new GridBagConstraints();
		
		//match ID
		gbc1.gridx = 0;
		gbc1.gridy = 0;
		gbc1.insets = new Insets(5, 0, 0, 10);
		gbc1.anchor = GridBagConstraints.LINE_END;
		preMatchDataPanel.add(lblMatchID, gbc1);
		
		gbc1.gridx = 1;
		gbc1.gridy = 0;
		gbc1.anchor = GridBagConstraints.LINE_START;
		preMatchDataPanel.add(matchID, gbc1);
		
		matchIDError.setForeground(Color.RED);
		matchIDError.setVisible(false);
		gbc1.gridx = 2;
		gbc1.gridy = 0;
		gbc1.anchor = GridBagConstraints.LINE_START;
		preMatchDataPanel.add(matchIDError, gbc1);
		
		//referee role		
		gbc1.gridx = 0;
		gbc1.gridy = 1;
		gbc1.insets = new Insets(5, 0, 0, 10);
		gbc1.anchor = GridBagConstraints.LINE_END;
		preMatchDataPanel.add(lblRole, gbc1);
			
		gbc1.gridx = 1;
		gbc1.gridy = 1;
		gbc1.anchor = GridBagConstraints.LINE_START;
		preMatchDataPanel.add(refRole, gbc1);
		
		roleError.setForeground(Color.RED);
		roleError.setVisible(false);
		gbc1.gridx = 2;
		gbc1.gridy = 1;
		gbc1.anchor = GridBagConstraints.LINE_START;
		preMatchDataPanel.add(roleError, gbc1);
		
		//team1		
		gbc1.gridx = 0;
		gbc1.gridy = 2;
		gbc1.insets = new Insets(5, 0, 0, 10);
		gbc1.anchor = GridBagConstraints.LINE_END;
		preMatchDataPanel.add(lblTeam1, gbc1);
		
		gbc1.gridx = 1;
		gbc1.gridy = 2;
		gbc1.anchor = GridBagConstraints.LINE_START;
		preMatchDataPanel.add(team1Name, gbc1);			
		
		team1Error.setForeground(Color.RED);
		team1Error.setVisible(false);
		gbc1.gridx = 2;
		gbc1.gridy = 2;
		gbc1.anchor = GridBagConstraints.LINE_START;
		preMatchDataPanel.add(team1Error, gbc1);
		
		//team2		
		gbc1.gridx = 0;
		gbc1.gridy = 3;
		gbc1.insets = new Insets(5, 0, 0, 10);
		gbc1.anchor = GridBagConstraints.LINE_END;
		preMatchDataPanel.add(lblTeam2, gbc1);
				
		gbc1.gridx = 1;
		gbc1.gridy = 3;
		gbc1.anchor = GridBagConstraints.LINE_START;
		preMatchDataPanel.add(team2Name, gbc1);	
				
		team2Error.setForeground(Color.RED);
		team2Error.setVisible(false);
		gbc1.gridx = 2;
		gbc1.gridy = 3;
		gbc1.anchor = GridBagConstraints.LINE_START;
		preMatchDataPanel.add(team2Error, gbc1);
		
		//league		
		gbc1.gridx = 0;
		gbc1.gridy = 4;
		gbc1.insets = new Insets(5, 0, 0, 10);
		gbc1.anchor = GridBagConstraints.LINE_END;
		preMatchDataPanel.add(lblLeague, gbc1);
		
		matchLeague.setModel(new DefaultComboBoxModel<>(League.values()));
		gbc1.gridx = 1;
		gbc1.gridy = 4;
		gbc1.anchor = GridBagConstraints.LINE_START;
		preMatchDataPanel.add(matchLeague, gbc1);	
		
		//match date		
		gbc1.gridx = 0;
		gbc1.gridy = 5;
		gbc1.insets = new Insets(5, 0, 0, 10);
		gbc1.anchor = GridBagConstraints.LINE_END;
		preMatchDataPanel.add(lblMatchDate, gbc1);
				
		gbc1.gridx = 1;
		gbc1.gridy = 5;
		gbc1.anchor = GridBagConstraints.LINE_START;
		matchDate.setDateToToday();
		preMatchDataPanel.add(matchDate, gbc1);
				
		dateError.setForeground(Color.RED);
		dateError.setVisible(false);
		gbc1.gridx = 2;
		gbc1.gridy = 5;
		gbc1.anchor = GridBagConstraints.LINE_START;
		preMatchDataPanel.add(dateError, gbc1);
		
		//match time		
		gbc1.gridx = 0;
		gbc1.gridy = 6;
		gbc1.insets = new Insets(5, 0, 0, 10);
		gbc1.anchor = GridBagConstraints.LINE_END;
		preMatchDataPanel.add(lblMatchTime, gbc1);
				
		gbc1.gridx = 1;
		gbc1.gridy = 6;
		gbc1.anchor = GridBagConstraints.LINE_START;
		preMatchDataPanel.add(matchTime, gbc1);
				
		timeError.setForeground(Color.RED);
		timeError.setVisible(false);
		gbc1.gridx = 2;
		gbc1.gridy = 6;
		gbc1.anchor = GridBagConstraints.LINE_START;
		preMatchDataPanel.add(timeError, gbc1);
		
		//match location		
		gbc1.gridx = 0;
		gbc1.gridy = 7;
		gbc1.insets = new Insets(5, 0, 0, 10);
		gbc1.anchor = GridBagConstraints.LINE_END;
		preMatchDataPanel.add(lblMatchLocation, gbc1);
				
		gbc1.gridx = 1;
		gbc1.gridy = 7;
		gbc1.anchor = GridBagConstraints.LINE_START;
		preMatchDataPanel.add(matchLocation, gbc1);
				
		placeError.setForeground(Color.RED);
		placeError.setVisible(false);
		gbc1.gridx = 2;
		gbc1.gridy = 7;
		gbc1.anchor = GridBagConstraints.LINE_START;
		preMatchDataPanel.add(placeError, gbc1);
		
		//match distance		
		gbc1.gridx = 0;
		gbc1.gridy = 8;
		gbc1.insets = new Insets(5, 0, 0, 10);
		gbc1.anchor = GridBagConstraints.LINE_END;
		preMatchDataPanel.add(lblMatchDistance, gbc1);
				
		gbc1.gridx = 1;
		gbc1.gridy = 8;
		gbc1.anchor = GridBagConstraints.LINE_START;
		preMatchDataPanel.add(matchDistance, gbc1);
				
		distanceError.setForeground(Color.RED);
		distanceError.setVisible(false);
		gbc1.gridx = 2;
		gbc1.gridy = 8;
		gbc1.anchor = GridBagConstraints.LINE_START;
		preMatchDataPanel.add(distanceError, gbc1);
		
		//match fee		
		gbc1.gridx = 0;
		gbc1.gridy = 9;
		gbc1.insets = new Insets(5, 0, 0, 10);
		gbc1.anchor = GridBagConstraints.LINE_END;
		preMatchDataPanel.add(lblMatchFee, gbc1);
				
		gbc1.gridx = 1;
		gbc1.gridy = 9;
		gbc1.anchor = GridBagConstraints.LINE_START;
		preMatchDataPanel.add(matchFee, gbc1);
				
		feeError.setForeground(Color.RED);
		feeError.setVisible(false);
		gbc1.gridx = 2;
		gbc1.gridy = 9;
		gbc1.anchor = GridBagConstraints.LINE_START;
		preMatchDataPanel.add(feeError, gbc1);
	}

	@Override
	public void buildLayoutPost() {
		GridBagConstraints gbc = new GridBagConstraints(); 

		//team1 goals
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(5, 0, 0, 10);
		gbc.anchor = GridBagConstraints.LINE_END;
		postMatchPanel1.add(team1Goals, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.anchor = GridBagConstraints.LINE_START;
		postMatchPanel1.add(goals1Count, gbc);	
		
		//team2 goals
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.anchor = GridBagConstraints.LINE_END;
		gbc.insets = new Insets(15, 0, 0, 10);
		postMatchPanel2.add(team2Goals, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.anchor = GridBagConstraints.LINE_START;
		postMatchPanel2.add(goals2Count, gbc);
		
		//team1 yellow cards
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.anchor = GridBagConstraints.LINE_END;
		gbc.insets = new Insets(15, 0, 0, 10);
		postMatchPanel1.add(team1YCards, gbc);

		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.anchor = GridBagConstraints.LINE_START;
		postMatchPanel1.add(yCards1Count, gbc);
		
		//team2 yellow cards		
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.anchor = GridBagConstraints.LINE_END;
		gbc.insets = new Insets(15, 0, 0, 10);
		postMatchPanel2.add(team2YCards, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.anchor = GridBagConstraints.LINE_START;
		postMatchPanel2.add(yCards2Count, gbc);
		
		//team1 red cards
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.anchor = GridBagConstraints.LINE_END;
		gbc.insets = new Insets(15, 0, 0, 10);
		postMatchPanel1.add(team1RCards, gbc);

		
		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.anchor = GridBagConstraints.LINE_START;
		postMatchPanel1.add(rCards1Count, gbc);
		
		//team2 red cards		
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.anchor = GridBagConstraints.LINE_END;
		gbc.insets = new Insets(15, 0, 0, 10);
		postMatchPanel2.add(team2RCards, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.anchor = GridBagConstraints.LINE_START;
		postMatchPanel2.add(rCards2Count, gbc);
		
		//penalty shoots team1
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.anchor = GridBagConstraints.LINE_END;
		gbc.insets = new Insets(15, 0, 0, 10);
		postMatchPanel1.add(team1Penalty, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 3;
		gbc.anchor = GridBagConstraints.LINE_START;
		postMatchPanel1.add(penalty1Count, gbc);
		
		//penalty shoots team1
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.anchor = GridBagConstraints.LINE_END;
		gbc.insets = new Insets(15, 0, 0, 10);
		postMatchPanel2.add(team2Penalty, gbc);

		gbc.gridx = 1;
		gbc.gridy = 3;
		gbc.anchor = GridBagConstraints.LINE_START;
		postMatchPanel2.add(penalty2Count, gbc);
	
	}

	@Override
	public boolean checkPostMatchParams() {
		return (Integer) goals1Count.getValue() != 0 ||	(Integer) goals2Count.getValue() != 0 ||
				(Integer) yCards1Count.getValue() != 0 || (Integer) yCards2Count.getValue() != 0 ||
				(Integer) rCards1Count.getValue() != 0 || (Integer) rCards2Count.getValue() != 0 ||
				(Integer) valuePenalty1.getValue() != 0 || (Integer) valuePenalty2.getValue() != 0;
	}
	
}
