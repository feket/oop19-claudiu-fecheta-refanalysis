package controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import controller.interfaces.IController;
import model.League;
import model.Match;
import model.Model;
import model.Result;
import model.Team;
import view.interfaces.IView;

/**
 * @author Claudiu Fecheta
 *
 */
public class Controller implements IController {
	
	private Match match;
	@SuppressWarnings("unused")
	private final Model model;
	private String file;
	private final Set<IView> view;
	private Team team1;
	private Team team2;
	private Result result;
	
	public Controller() {
		this.model = new Model();
		this.view = new HashSet<>();
	}

	@Override
	public void commandQuit() {
		System.exit(0);
	}

	@Override
	public void setFileName(String fileName) {
		this.file = fileName;
	}

	@Override
	public void addView(IView v) {
		v.attachViewObserver(this);
		view.add(v);		
	}

	@Override
	public void dataLoad() throws IOException {
		String matchID, teamName1, teamName2, role, league, time, place;
		int distance, matchFee;
		int goals1, goals2, yCards1, yCards2, rCards1, rCards2, penaltyKicks1, penaltyKicks2;
		Date date;
		StringTokenizer tokenizer;
		final BufferedReader read = new BufferedReader(new FileReader(
				file));
		String s = read.readLine();
		if (s == null) {
			read.close();
		} else {
			do {
				tokenizer = new StringTokenizer(s, ";");
				matchID = tokenizer.nextToken();
				role = tokenizer.nextToken();
				teamName1 = tokenizer.nextToken();
				teamName2 = tokenizer.nextToken();
				league = tokenizer.nextToken();
				date = Model.getDateFromString(tokenizer.nextToken());
				time = tokenizer.nextToken();
				place = tokenizer.nextToken();
				distance = Integer.parseInt(tokenizer.nextToken());
				matchFee = Integer.parseInt(tokenizer.nextToken());
				goals1 = Integer.parseInt(tokenizer.nextToken());
				goals2 = Integer.parseInt(tokenizer.nextToken());
				yCards1 = Integer.parseInt(tokenizer.nextToken());
				yCards2 = Integer.parseInt(tokenizer.nextToken());
				rCards1 = Integer.parseInt(tokenizer.nextToken());
				rCards2 = Integer.parseInt(tokenizer.nextToken());
				penaltyKicks1 = Integer.parseInt(tokenizer.nextToken());
				penaltyKicks2 = Integer.parseInt(tokenizer.nextToken());
				match = new Match(matchID);
				team1 = new Team();
				team1.setTeamName(teamName1);
				team2 = new Team();
				team2.setTeamName(teamName2);
				result = new Result(goals1, goals2);
				result.setYellowCards1(yCards1);
				result.setYellowCards2(yCards2);
				result.setRedCards1(rCards1);
				result.setRedCards2(rCards2);
				result.setPenaltyKicks1(penaltyKicks1);
				result.setPenaltyKicks2(penaltyKicks2);
				match.setRole(role);
				match.setTeam1(team1);
				match.setTeam2(team2);
				match.setLeague(League.valueOf(league)); 
				match.setDate(date);
				match.setTime(time);
				match.setPlace(place);
				match.setDistance(distance);
				match.setMatchFee(matchFee);
				match.setResult(result);
				Model.getLista().add(match);
				s = read.readLine();
			} while (s != null);
			read.close();
		}
		
	}

}
