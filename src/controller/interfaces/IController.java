package controller.interfaces;

import java.io.IOException;

import view.interfaces.IView;

/**
 * @author Claudiu Fecheta
 *
 */
public interface IController {
	
	/**
	 * Shuts down the program.
	 */
	void commandQuit();
	
	/**
	 * Sets the name of the file 
	 * @param fileName
	 */
	void setFileName(String fileName);
	
	/** 
	 * Add a view to this controller
	 * @param view
	 */
	void addView(IView view);
	
	/**
	 * Loads the list of the matches from the file.
	 * @throws IOException
	 */
	void dataLoad() throws IOException;

}
