package main;

import java.io.File;
import java.io.IOException;

import controller.Controller;
import view.MainView;

/**
 * @author Claudiu Fecheta
 *
 */
public class Start {

	private static final String HOME = System.getProperty("user.home");
	private static final String SEPARATOR = System.getProperty("file.separator");
	private static final String FILE_NAME = "partite.dat";
    private static File file = new File(HOME + SEPARATOR + FILE_NAME);

	/**
	 * Main method. This is where the application starts.
	 * 
	 * @param args
	 *            the arguments
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static void main(final String[] args) throws IOException,
			ClassNotFoundException {
		final Controller controller = new Controller();		
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		controller.setFileName(HOME + SEPARATOR + FILE_NAME);
		final MainView view = new MainView();
		controller.addView(view);
		controller.dataLoad();
	}

	/**
	 * @return the fileName
	 */
	public static String getFileName() {
		return HOME + SEPARATOR + FILE_NAME;
	}
}
